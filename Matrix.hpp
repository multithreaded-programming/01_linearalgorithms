#ifndef INC_01_LINEARALGORITHMS_MATRIX_HPP
#define INC_01_LINEARALGORITHMS_MATRIX_HPP

#include <utility>
#include <exception>

template<typename T>
class Matrix {
public:
    class Iterator{
        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = T*;
        using reference = T&;

    public:
        Iterator(pointer ptr) : _ptr(ptr) {}

        reference operator*() const {
            return *_ptr; }

        pointer operator->() {
            return _ptr; }

        Iterator &operator++() {
            ++_ptr; return *this; }

        Iterator operator++(int) {
            auto tmp = *this; ++_ptr; return tmp; }

        friend bool operator== (const Iterator &first, const Iterator &other) {
            return first._ptr == other._ptr; }

        friend bool operator!= (const Iterator &first, const Iterator &other) {
            return ! (first == other); }

    private:
        pointer _ptr;
    };

    using TSize = std::pair<std::size_t, std::size_t>;

    Matrix(const TSize &size, T defValue);
    Matrix(const TSize &size, std::initializer_list<T> initList);
    Matrix(const Matrix &other);

    ~Matrix();

    std::size_t GetElAmount() const;
    TSize Size() const;

    T &At(const TSize &pos);
    T At(const TSize &pos) const;

    T &operator[](std::size_t index);
    T operator[](std::size_t index) const;

    Matrix<T> operator*(const Matrix &other) const;

    Iterator Begin() { return Iterator(_matrix); }
    Iterator End() { return Iterator(_matrix + _elAmount); }

private:
    explicit Matrix(TSize size);

    const TSize _size;
    const std::size_t _elAmount;

    T *_matrix;
};

template<typename T>
Matrix<T>::Matrix(const Matrix::TSize &size, T defValue) : Matrix(size) {
    for (std::size_t i = 0; i < _elAmount; ++i) {
        _matrix[i] = defValue;
    }
}

template<typename T>
Matrix<T>::Matrix(const Matrix::TSize &size, std::initializer_list<T> initList)
        : Matrix(size) {
    for (std::size_t i = 0; auto &el: initList) {
        _matrix[i] = el;
        ++i;
    }
}

template<typename T>
Matrix<T>::Matrix(const Matrix &other) : Matrix(other._size) {
    for (std::size_t i = 0; i < _elAmount; ++i) {
        _matrix[i] = other._matrix[i];
    }
}

template<typename T>
std::size_t Matrix<T>::GetElAmount() const {
    return _elAmount;
}

template<typename T>
Matrix<T>::~Matrix() {
    delete[] _matrix;
}

template<typename T>
T &Matrix<T>::At(const Matrix::TSize &pos) {
    return _matrix[pos.first * _size.second + pos.second];
}

template<typename T>
T Matrix<T>::At(const Matrix::TSize &pos) const {
    return _matrix[pos.first * _size.second + pos.second];
}

template<typename T>
T &Matrix<T>::operator[](std::size_t index) {
    return _matrix[index];
}

template<typename T>
T Matrix<T>::operator[](std::size_t index) const {
    return _matrix[index];
}

template<typename T>
std::pair<std::size_t, std::size_t> Matrix<T>::Size() const {
    return _size;
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const Matrix &other) const {
    if (_size.second != other._size.first) {
        throw std::exception("Matrix::mul matrix aren't compatible");
    }

    auto res = Matrix({_size.first, other._size.second}, 0);

    for (std::size_t i = 0; i < res._size.first; ++i) {
        for (std::size_t j = 0; j < res._size.second; ++j) {
            for (std::size_t r = 0; r < _size.second; ++r) {
                res.At({i, j}) += At({i, r}) * other.At({r, j});
            }
        }
    }

    return res;
}

template<typename T>
Matrix<T>::Matrix(Matrix::TSize size)
        : _size{std::move(size)},
          _elAmount{_size.first * _size.second} {
    _matrix = new T[_elAmount];
}


#endif //INC_01_LINEARALGORITHMS_MATRIX_HPP
