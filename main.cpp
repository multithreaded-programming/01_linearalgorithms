#include <iostream>
#include <random>
#include <iomanip>

#include "SimpleUniformIntRND.hpp"
#include "Matrix.hpp"

void print(const Matrix<int> &matrix);

int main() {
    auto m1 = Matrix{{3, 2}, 0};
    auto m2 = Matrix{{2, 4}, 0};

    FillWithRandomIntValues<Matrix<int>::Iterator, int>(m1.Begin(), m1.End(),
                                                        {0,5});
    FillWithRandomIntValues<Matrix<int>::Iterator, int>(m2.Begin(), m2.End(),
                                                        {1, 6});

    print(m1);
    print(m2);
    print(m1 * m2);

    return 0;
}

void print(const Matrix<int> &matrix) {
    for (int i = 0; i < matrix.Size().first; ++i) {
        for (int j = 0; j < matrix.Size().second; ++j) {
            std::cout << std::setw(2) << matrix.At({i, j}) << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << std::string(10, '=') << std::endl;
}
