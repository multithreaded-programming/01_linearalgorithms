#ifndef INC_01_LINEARALGORITHMS_SIMPLEUNIFORMINTRND_HPP
#define INC_01_LINEARALGORITHMS_SIMPLEUNIFORMINTRND_HPP

template<typename T>
class SimpleUniformIntRND {
public:
    SimpleUniformIntRND(T minIncluded, T maxIncluded) {
        std::random_device r;

        _engine = std::default_random_engine(r());
        _distribution =
                std::uniform_int_distribution<T>(minIncluded, maxIncluded);
    }

    T GetValue() {
        return _distribution(_engine);
    }

private:
    std::default_random_engine _engine;

    std::uniform_int_distribution<T> _distribution;
};

template<class ForwardIter, typename T>
void FillWithRandomIntValues(ForwardIter begin, ForwardIter end,
                             const std::pair<T, T> &range) {
    auto generator = SimpleUniformIntRND<T>(range.first, range.second);

    for (; begin != end; ++begin) {
        *begin = generator.GetValue();
    }
}

#endif //INC_01_LINEARALGORITHMS_SIMPLEUNIFORMINTRND_HPP
